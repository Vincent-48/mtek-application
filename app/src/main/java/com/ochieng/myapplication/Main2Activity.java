package com.ochieng.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Main2Activity extends AppCompatActivity {
    private ImageView imageView5;
    private ImageView imageView6;
    private ImageView imageView13;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        imageView5 = (ImageView) findViewById(R.id.imageView5);
        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity_main3();
            }
        });
        imageView6 = (ImageView) findViewById(R.id.imageView6);
        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity_main4();
            }
        });
        imageView13 = (ImageView) findViewById(R.id.imageView13);
        imageView13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity_main5();
            }
        });
    }
    public void openActivity_main3(){
        Intent intent = new Intent(this,Main3Activity.class);
        startActivity(intent);
}
public void openActivity_main4(){
        Intent intent = new Intent(this,Main4Activity.class);
        startActivity(intent);
}
public void openActivity_main5(){
        Intent intent = new Intent(this,Main5Activity.class);
        startActivity(intent);
}
}

