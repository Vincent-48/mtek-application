package com.ochieng.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.app.Notification;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.MenuItem;

import com.google.android.gms.common.ErrorDialogFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class Main6Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);
        final BottomNavigationView navigationView = findViewById(R.id.btm_nav);

        navigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem menuItem) {

                int id = menuItem.getItemId();

                if (id == R.id.nav_profile) {
                    profile fragment = new profile();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, fragment);
                    fragmentTransaction.commit();

                    if (id == R.id.nav_notification) {
                        notification fragments = new notification();
                        FragmentTransaction fragmentTransaction6 = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.frame_layout, fragment);
                        fragmentTransaction.commit();

                        if (id == R.id.nav_error) {
                            emergency fragment3 = new emergency();
                            FragmentTransaction fragmentTransaction3 = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.frame_layout, fragment);
                            fragmentTransaction.commit();


                            if (id == R.id.nav_home) {
                                home fragment2 = new home();
                                FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.frame_layout, fragment2);
                                fragmentTransaction.commit();


                                if (id == R.id.nav_settings) {
                                    settings fragment4 = new settings();
                                    FragmentTransaction fragmentTransaction4 = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame_layout, fragment2);
                                    fragmentTransaction.commit();


                                }

                                return;
                            }

                            navigationView.setSelectedItemId(R.id.nav_profile);
                        }


                    }    }
            }
        }
                    );}
}